const assert = require('assert');

const productos = [
    {
        nombre: 'Capuccino',
        precio: 200
    },
    {
        nombre: 'Latte',
        precio: 50
    },
    {
        nombre: 'Chocolate',
        precio: 10
    }
]

const totalProductoConDescuento = (productos, descuento) => {
    return productos[0].precio - ((productos[0].precio * descuento) / 100); 
}

let tenner = totalProductoConDescuento(productos, 20); 
assert.strictEqual(tenner, 160); 

function carritoCompras(productos) {
    let totalCarrito = 0;
    productos.forEach(producto => {
        totalCarrito += producto.precio;
    })
    return totalCarrito;
}

let tennerCarrito = carritoCompras(productos);
assert.strictEqual(tennerCarrito, 260);