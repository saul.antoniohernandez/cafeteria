const assert = require('assert');

const productos = [
    {
        nombre: 'Capuccino',
        precio: 200
    },
    {
        nombre: 'Latte',
        precio: 50
    },
    {
        nombre: 'Chocolate',
        precio: 10
    }
]

const totalProductoConDescuento = (productos, descuento) => {
    let cantidadADescontar = (productos[0].precio * descuento) / 100; 
    let total = productos[0].precio - cantidadADescontar; 
    return total; 
}

let tenner = totalProductoConDescuento(productos, 20); 
assert.strictEqual(tenner, 160); 


const totalCarrito = (productos) => {
    let total = 0;
    for (let i = 0; i < productos.length; i++) {
        total += productos[i].precio;
    }
    return total; 
}

let tenner2 = totalCarrito(productos); 
assert.strictEqual(tenner2, 260); 
